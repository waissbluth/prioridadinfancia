class CreateMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :members do |t|
      t.string :name
      t.string :email
      t.string :rut

      t.timestamps
    end
    add_index :members, :email
    add_index :members, :rut, unique: true
  end
end
