class AddIndicesToMembers < ActiveRecord::Migration[5.0]
  def change
  	add_index :members, :created_at, order: {created_at: :desc}
  	add_index :members, :name, order: {name: :asc}
  end
end
