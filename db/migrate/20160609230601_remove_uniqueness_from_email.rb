class RemoveUniquenessFromEmail < ActiveRecord::Migration[5.0]
  def change
  	remove_index :members, :email
  	add_index :members, :email
  end
end
