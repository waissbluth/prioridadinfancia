class CreatePages < ActiveRecord::Migration[5.0]
  def change
    create_table :pages do |t|
      t.string :title
      t.text :body
      t.string :tag

      t.timestamps
    end
    add_index :pages, :tag, unique: true
  end
end
