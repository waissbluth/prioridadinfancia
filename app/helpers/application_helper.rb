module ApplicationHelper

  def markdown(text)
	 Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true).render(text).html_safe
  end
end
