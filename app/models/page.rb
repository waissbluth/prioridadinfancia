class Page < ApplicationRecord
	validates :body, presence: true
	validates :tag, presence: true, uniqueness: true
	validates :title, presence: true
end
