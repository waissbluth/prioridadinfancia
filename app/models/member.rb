class Member < ApplicationRecord
  
  validates :name, presence: true, length: { minimum: 3 }
  validates :email, format: Devise::email_regexp, allow_blank: true
  validates :rut, rut: { message: "no es un RUT válido" }, presence: true, uniqueness: true
  rescue_from_duplicate :rut
  rescue_from_duplicate :email

  scope :by_creation_date, -> { order(created_at: :desc) }
  scope :by_name,  -> {order(name: :asc)}

  def rut=(value)
    value = value.gsub('.','')
    write_attribute(:rut, value)
  end

  def self.to_csv(options = {})
	CSV.generate(options) do |csv|
		csv << column_names
			all.each do |member|
				csv << member.attributes.values_at(*column_names)
			end
		end
	end

end
