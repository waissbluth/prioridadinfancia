json.array!(@members) do |member|
  json.extract! member, :id, :name, :email, :rut
  json.url member_url(member, format: :json)
end
