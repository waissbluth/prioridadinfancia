Rails.application.routes.draw do
  resources :pages
  resources :members, path: 'firmantes'
  devise_for :users, path: ''
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'firmas', to: 'homepage#signatures', as: 'signatures'
  root 'members#new'

  get 'admin', to: redirect('/sign_in')
end
